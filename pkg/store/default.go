// Package store implemets methods for accessing key-value store.
package store

import (
	"flag"
	"time"
)

// DefaultStore - Default key-value store.
var DefaultStore Store

// defaultConfig - Default redis store configuration.
var defaultConfig = &RedisStoreConfig{
	MaxIdle:           5,
	IdleTimeout:       240 * time.Second,
	BorrowTestTimeout: 60 * time.Second,
	ServiceName:       "store",
}

// InitFlags - Initializes flags in default FlagSet.
func InitFlags() {
	flag.IntVar(&defaultConfig.MaxIdle, "store-max-idle", defaultConfig.MaxIdle, "Maximum store idle connections")
	flag.DurationVar(&defaultConfig.IdleTimeout, "store-idle-timeout", defaultConfig.IdleTimeout, "Store idle connection timeout")
	flag.DurationVar(&defaultConfig.BorrowTestTimeout, "store-borrow-test-time", defaultConfig.BorrowTestTimeout,
		"Time after connection is pinged before reusing (in seconds)")
	flag.StringVar(&defaultConfig.ServiceAddr, "store-service-addr", "", "Store service address")
	flag.StringVar(&defaultConfig.ServiceName, "store-service-name", defaultConfig.ServiceName, "Store service name in registry")
}

// Init - Initializes default key-value store.
func Init() {
	if !flag.Parsed() {
		flag.Parse()
	}

	// Create new default store
	DefaultStore = NewSSDBStore(defaultConfig)
}

// Get - Gets value stored under key in DefaultStore.
func Get(key string) ([]byte, error) {
	return DefaultStore.Get(key)
}

// Set - Sets value under key in DefaultStore.
func Set(key string, value []byte) error {
	return DefaultStore.Set(key, value)
}

// SetTTL - Sets value under key with time to live.
func SetTTL(key string, value []byte, ttl int64) error {
	return DefaultStore.SetTTL(key, value, ttl)
}

// GetString - Gets string value stored under key in DefaultStore.
func GetString(key string) (string, error) {
	return DefaultStore.GetString(key)
}

// SetString - Sets string value under key in DefaultStore.
func SetString(key, value string) error {
	return DefaultStore.SetString(key, value)
}

// SetStringTTL - Sets string value under key with time to live.
func SetStringTTL(key string, value string, ttl int64) error {
	return DefaultStore.SetStringTTL(key, value, ttl)
}

// SetGet - Gets values stored in SET under key.
func SetGet(key string) ([]string, error) {
	return DefaultStore.SetGet(key)
}

// SetAdd - Adds value to SET under key.
func SetAdd(key, value string) error {
	return DefaultStore.SetAdd(key, value)
}

// Delete - Deletes key from DefaultStore.
func Delete(key string) error {
	return DefaultStore.Delete(key)
}
