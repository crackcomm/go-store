// Package store implemets methods for accessing key-value store.
package store

import "errors"

// Store - Key-Value store interface.
type Store interface {
	Get(key string) ([]byte, error)
	Set(key string, value []byte) error
	SetTTL(key string, value []byte, ttl int64) error

	GetString(key string) (value string, err error)
	SetString(key string, value string) error
	SetStringTTL(key string, value string, ttl int64) error

	// Add to SET
	SetAdd(key string, value string) error
	SetGet(key string) (set []string, err error)

	Delete(key string) error
}

// ErrNotFound - Error returned when value was not found.
var ErrNotFound = errors.New("Value was not found")

// ErrServiceNotFound - Error returned when store service was not found.
var ErrServiceNotFound = errors.New("Store Service was not found")
