// Package store implemets methods for accessing key-value store.
package store

import "github.com/garyburd/redigo/redis"

// SSDBStore - SSDB store.
type SSDBStore struct {
	*RedisStore
}

// NewSSDBStore - Creates new redis store.
func NewSSDBStore(config *RedisStoreConfig) Store {
	// Create new redis store
	redisStore := NewRedisStore(config).(*RedisStore)
	redisStore.ssdb = true
	return &SSDBStore{redisStore}
}

// SetAdd - Adds value to SET under key.
func (s *SSDBStore) SetAdd(key, value string) (err error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Get value
	_, err = conn.Do("ZSET", key, value, 1)
	return
}

// SetGet - Gets values stored in SET under key.
func (s *SSDBStore) SetGet(key string) ([]string, error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Get value
	value, err := conn.Do("ZRANGE", key, 0, -1)
	if err != nil {
		return nil, err
	}

	// If value is empty return not found error
	if value == nil {
		return nil, ErrNotFound
	}

	// Cast result to strings array
	return redis.Strings(value, nil)
}
