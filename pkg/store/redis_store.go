// Package store implemets methods for accessing key-value store.
package store

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/myodc/go-micro/registry"
)

// RedisStore - Key-Value store interface.
type RedisStore struct {
	Config *RedisStoreConfig
	Pool   *redis.Pool

	ssdb bool
}

// RedisStoreConfig - Redis store config.
type RedisStoreConfig struct {
	MaxIdle           int
	IdleTimeout       time.Duration
	BorrowTestTimeout time.Duration
	ServiceName       string
	ServiceAddr       string
}

// NewRedisStore - Creates new redis store.
func NewRedisStore(config *RedisStoreConfig) Store {
	// Create new redis store
	store := &RedisStore{
		Config: config,
	}

	// Create new redis pool
	store.Pool = &redis.Pool{
		Dial:         store.dial,
		MaxIdle:      store.Config.MaxIdle,
		IdleTimeout:  store.Config.IdleTimeout,
		TestOnBorrow: store.borrowTest,
	}

	return store
}

// Get - Gets value stored under key.
func (s *RedisStore) Get(key string) ([]byte, error) {
	// Cast result to byte array
	return redis.Bytes(s.get(key))
}

// Set - Sets value under key.
func (s *RedisStore) Set(key string, value []byte) (err error) {
	return s.set(key, value, 0)
}

// SetTTL - Sets value under key with time to live.
func (s *RedisStore) SetTTL(key string, value []byte, ttl int64) (err error) {
	return s.set(key, value, ttl)
}

// GetString - Gets string value stored under key.
func (s *RedisStore) GetString(key string) (string, error) {
	return redis.String(s.get(key))
}

// SetString - Sets string value under key.
func (s *RedisStore) SetString(key string, value string) (err error) {
	return s.set(key, value, 0)
}

// SetStringTTL - Sets string value under key with time to live.
func (s *RedisStore) SetStringTTL(key string, value string, ttl int64) (err error) {
	return s.set(key, value, ttl)
}

// SetAdd - Adds value to SET under key.
func (s *RedisStore) SetAdd(key, value string) (err error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Get value
	_, err = conn.Do("SADD", key, value)
	return
}

// SetGet - Gets values stored in SET under key.
func (s *RedisStore) SetGet(key string) ([]string, error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Get value
	value, err := conn.Do("SMEMBERS", key)
	if err != nil {
		return nil, err
	}

	// If value is empty return not found error
	if value == nil {
		return nil, ErrNotFound
	}

	// Cast result to strings array
	return redis.Strings(value, nil)
}

// get - Gets value stored under key.
func (s *RedisStore) get(key string) (value interface{}, err error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Get value
	value, err = conn.Do("GET", key)
	if err != nil {
		return
	}

	// If value is empty return not found error
	if value == nil {
		err = ErrNotFound
	}

	return
}

// set - Sets value under key.
func (s *RedisStore) set(key string, value interface{}, ttl int64) (err error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Set value under key
	if ttl <= 0 {
		_, err = conn.Do("SET", key, value)
	} else if s.ssdb {
		_, err = conn.Do("SETX", key, value, ttl)
	} else {
		_, err = conn.Do("SET", key, value, ttl)
	}

	return
}

// Delete - Deletes key.
func (s *RedisStore) Delete(key string) (err error) {
	// Get database connection from the pool
	conn := s.Pool.Get()
	defer conn.Close()

	// Delete key
	_, err = conn.Do("DEL", key)
	return
}

// borrowTest - Test ran when connection is borrowed from the pool.
func (s *RedisStore) borrowTest(c redis.Conn, t time.Time) (err error) {
	// If was used before boborrowTestTime just reuse it
	if time.Now().Sub(t) <= s.Config.BorrowTestTimeout {
		return
	}

	// Ping connection
	_, err = c.Do("PING")
	return
}

// dial - Dials to redis service.
func (s *RedisStore) dial() (conn redis.Conn, err error) {
	address, err := s.serviceAddr()
	if err != nil {
		return
	}

	// Connect to store
	conn, err = redis.Dial("tcp", address)
	return
}

func (s *RedisStore) serviceAddr() (address string, err error) {
	if len(s.Config.ServiceAddr) > 0 {
		return s.Config.ServiceAddr, nil
	}

	// Get store services from registry
	service, err := registry.GetService(s.Config.ServiceName)
	if err != nil {
		return
	}

	// Return error if service was not found
	if len(service.Nodes) == 0 {
		return "", ErrServiceNotFound
	}

	// Get random service
	n := rand.Int() % len(service.Nodes)
	node := service.Nodes[n]
	address = fmt.Sprintf("%s:%d", node.Address, node.Port)
	return
}
